provider "aws" {
    region = "${var.aws_region}"
}

resource "aws_security_group" "bastion" {
    name = "bastion"
    description = "Allow port 22 from all of the world"

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
	
    vpc_id = "${var.vpc_id}"

    tags {
        Name = "Bastion"
		CreatedBy = "${var.user_name}"
    }
}

resource "aws_instance" "bastion" {
    ami                         = "${var.ami}"
    instance_type               = "${var.instance_size}"
    key_name                    = "${var.aws_key_name}"
    subnet_id                   = "${var.public_subnet_id}"
    vpc_security_group_ids      = ["${aws_security_group.bastion.id}"]
    associate_public_ip_address = true

    tags {
        Name = "${var.vpc_name} Bastion"
 		Enviornment = "${var.env}"
		CreatedBy = "${var.user_name}"
    }

    connection {
        type = "ssh"
        user = "ec2-user"
        private_key = "${file(var.ssh_key_file)}"
    }
}

output "bastion_ip" {
    value = "${aws_instance.bastion.private_ip}"
}

output "bastion_public_ip" {
    value = "${aws_instance.bastion.public_ip}"
}